package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

import java.awt.*;
import java.io.IOException;

public class Controller {
    @FXML
    BorderPane mainPane;


    //public User antonio = new User("Antonio","antonio@gmail.com","antonio1234");


   public void button1 (ActionEvent event) throws IOException {

       FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("userspage.fxml"));
       mainPane.getChildren().remove(mainPane.getCenter());
       try {
           mainPane.setCenter(fxmlLoader.load());

       }catch (IOException e){
           e.printStackTrace();
       }


   }


    public void button2(ActionEvent event) {
       FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("productpage_.fxml"));
       mainPane.getChildren().remove(mainPane.getCenter());
       try {
           mainPane.setCenter(fxmlLoader.load());
       }catch (IOException e){
           e.printStackTrace();
       }
    }

    public void button3(ActionEvent event) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("tracingpage_.fxml"));
        mainPane.getChildren().remove(mainPane.getCenter());
        try {
            mainPane.setCenter(fxmlLoader.load());
        }catch (IOException e){
            e.printStackTrace();
        }

    }

    public void button4(ActionEvent event) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("about.fxml"));
        mainPane.getChildren().remove(mainPane.getCenter());
        try {
            mainPane.setCenter(fxmlLoader.load());
        }catch (IOException e){
            e.printStackTrace();
        }
    }



}
